"""Module containing the definition of the grammar used for parsing pandas domain utterances."""

from ..rules import makeRule, AnnotatorRule
from ..semantics import (ProxySemantic, CombineSemantic, SimpleSemantic, NestedCombineSemantic,
                         NestedSemantic, CustomSemantic)
from ..annotators import (TokenAnnotator, StringAnnotator, SimpleNumberAnnotator,
                          SeparatedTokenListAnnotator)
from ..grammar import Grammar

pandasRules = [
    # general rules
    makeRule("$ROOT", "$PandasQuery", ProxySemantic(0)),
    makeRule("$ROOT", "?$Text $PandasQuery ?$Text", ProxySemantic(1)),
    makeRule(
        "$PandasQuery",
        "$OperationWithArguments",
        CombineSemantic([0], additionals={"domain": "pandas"})
    ),
    makeRule(
        "$OperationWithArguments",
        "$Operation $OperationArgument",
        CombineSemantic([0, 1])
    ),
    makeRule(
        "$OperationWithArguments",
        "$OperationArgument $Operation",
        CombineSemantic([0, 1])
    ),
    makeRule(
        "$OperationWithArguments",
        "$OperationWithArguments $OperationArgument",
        CombineSemantic([0, 1])
    ),
    makeRule(
        "$OperationWithArguments",
        "$OperationArgument $OperationWithArguments",
        CombineSemantic([0, 1])
    ),

    # operation rules
    makeRule("$Operation", "$AppendOperation", ProxySemantic(0)),
    makeRule("$Operation", "$CreateOperation", ProxySemantic(0)),
    makeRule("$Operation", "$SelectOperation", ProxySemantic(0)),
    makeRule("$Operation", "$ConvertOperation", ProxySemantic(0)),
    makeRule("$Operation", "$SortOperation", ProxySemantic(0)),

    #   append operation
    makeRule("$AppendOperation", "$Append", SimpleSemantic({"operation": "append"})),

    #   create operation
    makeRule("$CreateOperation", "$Create", SimpleSemantic({"operation": "create"})),

    #   select operation
    makeRule("$SelectOperation", "$Select", SimpleSemantic({"operation": "select"})),

    #   convert operation
    makeRule("$ConvertOperation", "$Convert", SimpleSemantic({"operation": "convert"})),

    #   sort operation
    makeRule("$SortOperation", "$Sort", SimpleSemantic({"operation": "sort"})),

    # operation argument rules
    makeRule("$OperationArgument", "$TypeArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$NamedTypeArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$SelectTypeArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$FromVariableArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$SelectSourceArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$IndexArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$LogicalConditionArgument", NestedSemantic({"condition": 0})),
    makeRule("$OperationArgument", "$RangeArgument", NestedSemantic({"range": 0})),
    makeRule("$OperationArgument", "$OpenRangeArgument", NestedSemantic({"range": 0})),
    makeRule("$OperationArgument", "$HeadEntriesArgument", NestedSemantic({"head": 0})),
    makeRule("$OperationArgument", "$TailEntriesArgument", NestedSemantic({"tail": 0})),
    makeRule("$OperationArgument", "$ConvertFormatArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$AssignArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$AppendTypeArgument", ProxySemantic(0)),
    makeRule(
        "$OperationArgument",
        "$AppendSourceTargetArgument",
        NestedSemantic({"appendSourceTarget": 0})
    ),
    makeRule("$OperationArgument", "$OrderByArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$CombinedSelectSourceArgument", ProxySemantic(0)),


    #   type argument
    makeRule("$TypeArgument", "$Dataframe", SimpleSemantic({"type": "DataFrame"})),
    # makeRule("$TypeArgument", "$Series", SimpleSemantic({"type": "Series"})),

    #   named type argument
    makeRule(
        "$NamedTypeArgument",
        # NOTE: making the "TypeArgument" non optional drastically reduces the amount of possible
        # parses
        "?$TypeArgument $Variable",
        NestedCombineSemantic({"typeVariable": 1}, [0])
    ),

    #   select type argument
    makeRule("$SelectTypeArgument", "$Row", SimpleSemantic({"selectType": "row"})),
    makeRule(
        "$SelectTypeArgument",
        "$Column ?$Variable",
        NestedCombineSemantic({"columnName": 1}, [], additionals={"selectType": "column"})
    ),
    makeRule("$SelectTypeArgument", "$Maximum ?$Value", SimpleSemantic({"selectType": "maximum"})),
    makeRule("$SelectTypeArgument", "$Minimum ?$Value", SimpleSemantic({"selectType": "minimum"})),
    makeRule("$SelectTypeArgument", "$Average ?$Value", SimpleSemantic({"selectType": "average"})),

    #   select source argument
    makeRule(
        "$SelectSourceArgument",
        "$From $NamedTypeArgument",
        NestedSemantic({"selectSource": 1})
    ),
    makeRule(
        "$SelectSourceArgument",
        "$From $TypeArgument",
        NestedSemantic({"selectSource": 1})
    ),
    makeRule(
        "$SelectSourceArgument",
        "$Of $NamedTypeArgument",
        NestedSemantic({"selectSource": 1})
    ),
    makeRule(
        "$SelectSourceArgument",
        "$Of $TypeArgument",
        NestedSemantic({"selectSource": 1})
    ),
    makeRule(
        "$SelectSourceArgument",
        "$In $NamedTypeArgument",
        NestedSemantic({"selectSource": 1})
    ),
    makeRule(
        "$SelectSourceArgument",
        "$In $TypeArgument",
        NestedSemantic({"selectSource": 1})
    ),

    #   sub select source argument
    makeRule(
        "$SubSelectSourceArgument",
        "$SelectSourceArgument",
        CustomSemantic(lambda sems: {"subSelectSource": sems[0]["selectSource"]})
    ),

    #   combined select source argument
    makeRule(
        "$CombinedSelectSourceArgument",
        "$SubSelectSourceArgument $SelectSourceArgument",
        CombineSemantic([0, 1])
    ),
    makeRule(
        "$CombinedSelectSourceArgument",
        "$SelectSourceArgument $SubSelectSourceArgument",
        CombineSemantic([0, 1])
    ),

    #   from variable argument
    makeRule("$FromVariableArgument", "$From $Variable", NestedSemantic({"from": 1})),

    #   index argument
    makeRule("$IndexArgument", "$Using $Index ?$Token", NestedSemantic({"index": 2})),

    #   range argument
    makeRule("$RangeArgument", "?$From $Number $To $Number", NestedSemantic({"from": 1, "to": 3})),

    #   head/tail entries argument
    makeRule("$HeadEntriesArgument", "$First $Number", ProxySemantic(1)),
    makeRule("$TailEntriesArgument", "$Last $Number", ProxySemantic(1)),

    #   logical condition argument
    makeRule(
        "$LogicalConditionArgument",
        "$Where $Variable $ComparisionOperator ?$Than $Variable",
        NestedCombineSemantic({"lhs": 1, "rhs": 4}, [2])
    ),
    makeRule(
        "$LogicalConditionArgument",
        "$Where $Variable $ComparisionOperator ?$To $Variable",
        NestedCombineSemantic({"lhs": 1, "rhs": 4}, [2])
    ),

    #   convert format argument
    makeRule("$ConvertFormatArgument", "$To $Format", NestedSemantic({"convertFormat": 1})),

    #   assign argument
    makeRule("$AssignArgument", "$As $Variable", NestedSemantic({"assignTo": 1})),
    makeRule("$AssignArgument", "?$And $Assign $To $Variable", NestedSemantic({"assignTo": 3})),

    #   append type argument
    makeRule("$AppendTypeArgument", "$Row", SimpleSemantic({"appendType": "row"})),
    makeRule("$AppendTypeArgument", "$Column", SimpleSemantic({"appendType": "column"})),

    #   append targets argument
    makeRule(
        "$AppendSourceTargetArgument",
        "?$Of $Variable $To $Variable",
        NestedSemantic({"source": 1, "target": 3})
    ),
    makeRule(
        "$AppendSourceTargetArgument",
        "?$Of $Variable $And $Variable",
        NestedSemantic({"source": 1, "target": 3})
    ),

    #   order by argument
    makeRule(
        "$OrderByArgument",
        "$By $Variable",
        NestedSemantic({"orderBy": 1})
    ),
    makeRule(
        "$OrderByArgument",
        "$Using $Variable",
        NestedSemantic({"orderBy": 1})
    ),

    # logical operator rules
    makeRule("$ComparisionOperator", "$LogicalGreater", SimpleSemantic({"operator": ">"})),
    makeRule("$ComparisionOperator", "$LogicalGreaterEqual", SimpleSemantic({"operator": ">="})),
    makeRule("$ComparisionOperator", "$LogicalLess", SimpleSemantic({"operator": "<"})),
    makeRule("$ComparisionOperator", "$LogicalLessEqual", SimpleSemantic({"operator": "<="})),
    makeRule("$ComparisionOperator", "$LogicalEqual", SimpleSemantic({"operator": "=="})),
    makeRule("$ComparisionOperator", "$LogicalNotEqual", SimpleSemantic({"operator": "!="})),


    # misc terminal rules

    # Will become operations
    makeRule("$Append", "append"),
    makeRule("$Append", "concat"),
    makeRule("$Append", "attach"),

    makeRule("$Create", "create"),
    makeRule("$Create", "new"),

    makeRule("$Convert", "convert"),
    makeRule("$Convert", "transform"),
    makeRule("$Convert", "cast"),

    makeRule("$Select", "select"),
    makeRule("$Select", "retrieve"),
    makeRule("$Select", "get"),

    makeRule("$Sort", "sort"),
    makeRule("$Sort", "order"),

    makeRule("$Csv", "csv"),

    makeRule("$Dict", "dict"),

    makeRule("$Json", "json"),

    makeRule("$Numpy", "numpy"),

    makeRule("$Where", "where"),

    makeRule("$From", "from"),

    makeRule("$To", "to"),
    makeRule("$To", "into"),

    makeRule("$By", "by"),

    makeRule("$And", "and"),

    makeRule("$Or", "or"),

    makeRule("$On", "on"),

    makeRule("$Is", "is"),

    makeRule("$Of", "of"),

    makeRule("$In", "in"),

    makeRule("$As", "as"),

    makeRule("$Than", "than"),

    makeRule("$Assign", "assign"),

    makeRule("$Using", "using"),
    makeRule("$Using", "with"),
    makeRule("$Using", "$Based $On"),

    makeRule("$Based", "based"),

    makeRule("$Dataframe", "dataframe"),
    # makeRule("$Series", "series"),

    makeRule("$Row", "row"),
    makeRule("$Row", "rows"),
    makeRule("$Row", "entry"),
    makeRule("$Row", "entries"),

    makeRule("$Column", "column"),
    makeRule("$Column", "columns"),

    makeRule("$Index", "index"),

    makeRule("$Value", "value"),
    makeRule("$Value", "values"),

    makeRule("$Biggest", "biggest"),

    makeRule("$Smallest", "smallest"),

    makeRule("$Maximum", "maximum"),
    makeRule("$Maximum", "maxima"),
    makeRule("$Maximum", "$Biggest"),

    makeRule("$Minimum", "minimum"),
    makeRule("$Minimum", "minima"),
    makeRule("$Minimum", "$Smallest"),

    makeRule("$Average", "average"),
    makeRule("$Average", "mean"),

    makeRule("$First", "first"),
    makeRule("$First", "top"),

    makeRule("$Last", "last"),
    makeRule("$Last", "bottom"),

    makeRule("$Not", "not"),

    makeRule("$LogicalGreater", "greater"),
    makeRule("$LogicalGreater", "is greater"),
    makeRule("$LogicalGreater", ">"),

    makeRule("$LogicalLess", "less"),
    makeRule("$LogicalLess", "is less"),
    makeRule("$LogicalLess", "<"),

    makeRule("$LogicalEqual", "equal"),
    makeRule("$LogicalEqual", "is equal"),
    makeRule("$LogicalEqual", "=="),

    makeRule("$LogicalGreaterEqual", "$LogicalGreater ?$Or $LogicalEqual"),
    makeRule("$LogicalGreaterEqual", ">="),

    makeRule("$LogicalLessEqual", "$LogicalLess ?$Or $LogicalEqual"),
    makeRule("$LogicalLessEqual", "<="),

    makeRule("$LogicalNotEqual", "$Not $LogicalEqual"),
    makeRule("$LogicalNotEqual", "!="),

    # format rules
    makeRule("$Format", "$Csv", SimpleSemantic({"format": "csv"})),
    makeRule("$Format", "$Dict", SimpleSemantic({"format": "dict"})),
    makeRule("$Format", "$Json", SimpleSemantic({"format": "json"})),
    makeRule("$Format", "$Numpy", SimpleSemantic({"format": "numpy"})),

    # variables rules
    makeRule(
        "$Variable",
        "$Token",
        CustomSemantic(lambda sems: {"variable": sems[0]["token"]})
    ),
    makeRule(
        "$Variable",
        "$String",
        CustomSemantic(lambda sems: {"variable": sems[0]["string"]})
    ),
    makeRule(
        "$Variable",
        "$TokenList",
        CustomSemantic(lambda sems: {"variable": sems[0]["tokenList"]})
    ),


    # annotator rules
    AnnotatorRule("$TextToken", TokenAnnotator()),
    AnnotatorRule("$Token", TokenAnnotator()),
    AnnotatorRule("$TokenList", SeparatedTokenListAnnotator()),
    AnnotatorRule("$String", StringAnnotator()),
    AnnotatorRule("$Number", SimpleNumberAnnotator()),

    # "other" domain rules
    makeRule("$ROOT", "$OtherQuery", ProxySemantic(0)),
    makeRule("$OtherQuery", "$Text", SimpleSemantic({"domain": "other"})),
    makeRule("$Text", "$TextToken ?$Text"),




    # Extend grammar for benchmark 1

    # Implementing the operation Split, Remove and Replace
    makeRule("$Operation", "$SplitOperation", ProxySemantic(0)),
    makeRule("$Operation", "$ReplaceOperation", ProxySemantic(0)),
    makeRule("$Operation", "$RemoveOperation", ProxySemantic(0)),

    makeRule("$SplitOperation", "$Split", SimpleSemantic({"operation": "split"})),
    makeRule("ReplaceOperation", "$Replace", SimpleSemantic({"operation": "replace"})),
    makeRule("RemoveOperation", "$Remove", SimpleSemantic({"operation": "remove"})),

    makeRule("$Split", "split"),
    makeRule("$Split", "Split"),

    makeRule("$Replace", "replace"),
    makeRule("$Replace", "Replace"),

    makeRule("$Remove", "remove"),
    makeRule("$Remove", "Remove"),




    # Regex expressions


    makeRule("$OperationArgument", "$SelectedRegexArgument", ProxySemantic(0)),

    makeRule("$SelectRegexArgument", "$Whitespace ?$Value", SimpleSemantic({"selectRegex": "whitespace"})),
    makeRule("$SelectRegexArgument", "$Digits ?$Value", SimpleSemantic({"selectRegex": "digits"})),
    makeRule("$SelectRegexArgument", "$Uppercase ?$Value", SimpleSemantic({"selectRegex": "uppercase"})),
    makeRule("$SelectRegexArgument", "$Lowercase ?$Value", SimpleSemantic({"selectRegex": "lowercase"})),


    makeRule("$Whitespace", "one whitespace"),
    makeRule("$Digits", "all digits"),
    makeRule("$Uppercase", "uppercase"),
    makeRule("$Lowercase", "lowercase"),
]

pandasGrammar = Grammar(pandasRules, startSymbol="$ROOT")
