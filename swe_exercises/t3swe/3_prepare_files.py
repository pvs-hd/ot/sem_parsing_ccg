import os.path

from exercise_mgr.Settings import Settings
from exercise_mgr.populate import lyx_to_latex, split_all_tex


def main():
    paths = [Settings.files / str(i) for i in range(12, 20)]
    for directory in paths:
        if os.path.isdir(directory):
            files = directory.glob('*.lyx')
            (directory / "comment").mkdir(exist_ok=True)
            (directory / "split").mkdir(exist_ok=True)
            for file in files:
                lyx_to_latex.notes_to_comments(file, (directory / "comment" / file.name))
                lyx_to_latex.lyx_to_latex(str(directory / "comment" / file.name))
                split_all_tex.separate_exercises(str(directory / "comment" / (file.stem + ".tex")), (directory / "split"))
    # ueb1-ibn-2012.tex is an exception since it already is a tex file
    split_all_tex.separate_exercises(str(Settings.files / '12' / "ueb1-ibn-2012.tex"),
                                     str(Settings.files / '12' / "split"))


if __name__ == '__main__':
    main()