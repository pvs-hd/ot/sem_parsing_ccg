import unittest
import json


class TestT5swe(unittest.TestCase):
    def test1(self):
        with open("#IBN-Uebungsthemen (2010-2019).csv", 'r', encoding='utf-8') as file:
            a = 0
            topics = file.readlines()
            for topic in topics:
                topics_split = topic.split(';')
                if topics_split[0] == "Ü-12-1-5":
                    for row in topics_split:
                        if row == "X":
                            a += 1
        with open("topics.json", 'r', encoding='utf8') as topics_json:
            b = 0
            topics = json.load(topics_json)
            for topic in topics:
                if topic == "Ü-12-1-5":
                    b = len(topics[topic][1])
        self.assertEqual(a, b)

    def test2(self):
        with open("#IBN-Uebungsthemen (2010-2019).csv", 'r', encoding='utf-8') as file:
            accordances_csv = 0
            topics = file.readlines()
            for topic in topics:
                topics_split = topic.split(';')
                if topics_split[0] == "Ü-12-2-3":
                    length_scv = len(topics_split)
                    table_csv = [False] * length_scv
                    num = 0
                    for i in range(length_scv):
                        if topics_split[i] == "X":
                            table_csv[num] = True
                        if topics_split[i] and topics_split[i][0] != "Ü":
                            num += 1
                elif topics_split[0] == "Ü-12-3-1":
                    num = 0
                    for i in range(length_scv):
                        if table_csv[num] and topics_split[i] == "X":
                            accordances_csv += 1
                        if topics_split[i] and topics_split[i][0] != "Ü":
                            num += 1
        with open("topics.json", 'r', encoding='utf8') as topics_json:
            accordances_json = 0
            length_json = 100
            table_json = [False] * length_json
            topics = json.load(topics_json)
            for topic in topics:
                if topic == "Ü-12-2-3":
                    for i in range(len(topics[topic][1])):
                        table_json[int(topics[topic][1][i])] = True
                elif topic == "Ü-12-3-1":
                    for i in range(len(topics[topic][1])):
                        if table_json[int(topics[topic][1][i])]:
                            accordances_json += 1
        self.assertEqual(accordances_csv, accordances_json)


if __name__ == '__main__':
    unittest.main()
